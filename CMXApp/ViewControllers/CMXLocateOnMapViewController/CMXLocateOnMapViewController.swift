//
//  CMXLocateOnMapViewController.swift
//  CMXApp
//
//  Created by Cisco on 02/10/16.
//  Copyright © 2016 Cisco. All rights reserved.
//


import UIKit
import Foundation
import CMXSdk
import AFNetworking
import SDWebImage
import ZKPulseView

/// <#Description#>
class CMXLocateOnMapViewController: UIViewController , UIScrollViewDelegate {
    
    @IBOutlet weak var xLabel: UILabel!
    @IBOutlet weak var yLabel: UILabel!
    @IBOutlet weak var confidenceLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var connectedApLabel: UILabel!
    @IBOutlet var scrollView : UIScrollView!
    @IBOutlet var imageViewContainerView : UIView!
    @IBOutlet var imageView : UIImageView!
    
    var server : Server?
    var macAddressToTrack : String = ""
    var lastKnownLocation : CMXLocationData?
    var userLocationView = UIView(frame: CGRect(x: 0, y: 0, w: 18, h: 18))
    var mapImage : UIImage?
    var lastMapImageName = ""
    
    //MARK: - view controller life cycle methods
    
    /// <#Description#>
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layoutIfNeeded()
        startupInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    
    /// <#Description#>
    func setupForNavigationBar(){
        setupNavigationBarTitle("Track", viewController: self)
        addNavigationBarButton(self, image: UIImage(named: "close"), title: nil, isLeft: true)
    }
    
    /// <#Description#>
    func registerForNotifications(){
        
    }
    
    /// <#Description#>
    func startupInitialisations(){
        self.view.showActivityIndicator()
        self.userLocationView.makeMeRound()
        self.userLocationView.layer.borderColor = UIColor.white.cgColor
        self.userLocationView.layer.borderWidth = 2.0
        self.userLocationView.isHidden = true
        loginToServerAndStartFetchingLocationUpdate()
    }
    
    
    func reAskMacAddress(){
        if self.presentedViewController == nil {
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(CMXLocateOnMapViewController.loginToServerAndStartFetchingLocationUpdate), object: nil)
            CMXAppCommonFunctions.sharedInstance.getMacAddressAsInput(viewController: self, completionBlock: { (enteredText) in
                self.macAddressToTrack = enteredText as! String
                NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(CMXLocateOnMapViewController.loginToServerAndStartFetchingLocationUpdate), object: nil)
                self.perform(#selector(CMXLocateOnMapViewController.loginToServerAndStartFetchingLocationUpdate), with: nil, afterDelay: 1.0)
            })
        }
    }
    
    func loginToServerAndStartFetchingLocationUpdate(){
        CMXSdk.shared.authenticate(ip: server!.ip!, name: server!.name!, userName: server!.userName!, password: server!.password!, completion: {[weak self] (server) in  guard let `self` = self else { return }
            if isNotNull(server){
                CMXSdk.shared.startLocationUpdates(self.macAddressToTrack,5.0, {[weak self] (update) in guard let `self` = self else { return }
                    self.lastKnownLocation = update
                    if self.lastKnownLocation != nil {
                        self.updateParametersWithLocationUpdate()
                    }
                    }, failure: {[weak self] (error) in guard let `self` = self else { return }
                        showNotification(MESSAGE_TEXT___UNABLE_TO_TRACK_THIS_MAC_ADDRESS, showOnNavigation: false, showAsError: true)
                        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(CMXLocateOnMapViewController.reAskMacAddress), object: nil)
                        self.perform(#selector(CMXLocateOnMapViewController.reAskMacAddress), with: nil, afterDelay: 1.0)
                    })
            }
            }, failure: {[weak self] (error) in guard let `self` = self else { return }
                let prompt = UIAlertController(title: "Login Failed", message: MESSAGE_TEXT___UNABLE_TO_LOGIN_WITH_THIS_SERVER, preferredStyle: UIAlertControllerStyle.alert)
                prompt.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler:{[weak self] (action) -> Void in guard let `self` = self else { return }
                    self.closeAndPop()
                    }))
                prompt.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: {[weak self] (action) -> Void in guard let `self` = self else { return }
                    NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(CMXLocateOnMapViewController.loginToServerAndStartFetchingLocationUpdate), object: nil)
                    self.perform(#selector(CMXLocateOnMapViewController.loginToServerAndStartFetchingLocationUpdate), with: nil, afterDelay: 1.0)
                    }
                ))
                self.presentVC(prompt)
            })
    }
    
    /// <#Description#>
    func updateUserInterfaceOnScreen(){
        
    }
    
    func updateParametersWithLocationUpdate() {
        xLabel.text = safeString(object: lastKnownLocation!.mapCoordinate.x.toString)
        yLabel.text = safeString(object: lastKnownLocation!.mapCoordinate.y.toString)
        timeLabel.text = safeString(object: lastKnownLocation!.mapInfo.serverTimeStamp.dateValue()?.toString(dateStyle: .medium, timeStyle: .medium))
        connectedApLabel.text = safeString(object: server!.ip)
        confidenceLabel.text = safeString(object: lastKnownLocation?.mapCoordinate.confidenceFactor)
        if CMXSdk.shared.IsLoggedIn(){
            if self.lastMapImageName != lastKnownLocation!.mapInfo.floorImage.imageName {
                CMXSdk.shared.getImageData(lastKnownLocation!.mapInfo.floorImage.imageName) {[weak self] (imageData) in guard let `self` = self else { return }
                    if isNotNull(imageData){
                        self.lastMapImageName = self.lastKnownLocation!.mapInfo.floorImage.imageName
                        self.mapImage = UIImage(data: imageData as! Data)
                        self.imageViewContainerView.frame = CGRect(x: 0, y: 0, w: self.mapImage!.size.width, h: self.mapImage!.size.height)
                        self.imageView.frame = CGRect(x: 0, y: 0, w: self.mapImage!.size.width, h: self.mapImage!.size.height)
                        self.imageView.image = self.mapImage
                        self.view.hideActivityIndicator()
                        self.imageView.addSubview(self.userLocationView)
                        self.userLocationView.isHidden = false
                        self.scrollView.contentSize = self.imageView!.size
                        self.updateUserCurrentLocationOnScreen()
                        self.animateUserLocationViewAsRequired()
                    }
                }
            }
        }
        updateUserCurrentLocationOnScreen()
        animateUserLocationViewAsRequired()
    }
    
    func animateUserLocationViewAsRequired() {
        if self.lastKnownLocation != nil && mapImage != nil {
            let capturedAt = self.lastKnownLocation?.capturedAt
            if capturedAt != nil {
                if capturedAt!.compare(Date(timeIntervalSinceNow: -20)) == ComparisonResult.orderedAscending {
                    self.userLocationView.backgroundColor = APP_THEME_RED_COLOR
                    self.userLocationView.startPulse(with: APP_THEME_RED_COLOR, offset: CGSize(width: 0, height: 0), frequency: 1)
                }else{
                    self.userLocationView.backgroundColor = APP_THEME_COLOR
                    self.userLocationView.startPulse(with: APP_THEME_COLOR, offset: CGSize(width: 0, height: 0), frequency: 3)
                }
                performAnimatedEffectType2(self.userLocationView, repeatCount: 2)
            }
        }
    }
    
    func updateUserCurrentLocationOnScreen() {
        if self.lastKnownLocation != nil && mapImage != nil {
            print("imageView!.frame.size.width \(imageView!.frame.size.width)")
            let scaleMapImageToLocation = Double(mapImage!.size.width)/self.lastKnownLocation!.mapInfo.floorDimension.width
            let xValue = self.lastKnownLocation!.mapCoordinate.x * scaleMapImageToLocation
            let yValue = self.lastKnownLocation!.mapCoordinate.y * scaleMapImageToLocation
            UIView.animate(withDuration: 3, animations: {
                self.userLocationView.center = CGPoint(x: xValue, y: yValue)
            })
        }
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageViewContainerView
    }
    
    func closeAndPop(){
        CMXSdk.shared.stopLocationUpdates()
        CMXSdk.shared.logout()
        self.navigationController!.popViewController(animated: true)
    }
    
    func onClickOfLeftBarButton(){
        closeAndPop()
    }
    
    /// Description
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

