//
//  CMXAppSpecificConstants.h
//  CMXApp
//
//  Created by Cisco on 02/10/16.
//  Copyright (c) 2016 Cisco. All rights reserved.
//

import UIKit

// constants strings
let FONT_BOLD = "SFUIText-Bold"
let FONT_SEMI_BOLD = "SFUIText-Medium"
let FONT_REGULAR = "SFUIText-Light"
let APP_UNIQUE_BUILD_IDENTIFIER = "APP_UNIQUE_BUILD_IDENTIFIER"


// constants keys


// constants cache manager keys


// constants values
let ENABLE_LOGGING_WEB_SERVICE_RESPONSE = true
let MINOR_DELAY = 0.1
let MINIMUM_LENGTH_LIMIT_USERNAME = 1
let MAXIMUM_LENGTH_LIMIT_USERNAME = 32
let MINIMUM_LENGTH_LIMIT_FIRST_NAME = 0
let MAXIMUM_LENGTH_LIMIT_FIRST_NAME = 64
let MINIMUM_LENGTH_LIMIT_PASSWORD = 1
let MAXIMUM_LENGTH_LIMIT_PASSWORD = 20
let MINIMUM_LENGTH_LIMIT_MOBILE_NUMBER = 7
let MAXIMUM_LENGTH_LIMIT_MOBILE_NUMBER = 14
let MINIMUM_LENGTH_LIMIT_EMAIL = 7
let MAXIMUM_LENGTH_LIMIT_EMAIL = 64
let REQUEST_TIME_OUT = 90.0

// constant feature enable/disable flags

// constant urls

// constant feature enable/disable flags

// constant urls

// constants objects
let userDefaults = UserDefaults.standard
let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
let KDeviceWidth  = UIScreen.main.bounds.size.width as CGFloat
let KDeviceHeight = UIScreen .main.bounds.size.height as CGFloat

// constants color
let APP_THEME_COLOR = UIColor(red: 36/255, green: 131/255, blue: 228/255, alpha: 1.0)
let APP_THEME_RED_COLOR = UIColor(red: 230/255, green: 112/255, blue: 99/255, alpha: 1.0)

// constants descriptions
let MESSAGE_TEXT___FOR_NETWORK_NOT_REACHABILITY = "The Internet connection appears to be offline."
let MESSAGE_TEXT___FOR_SERVER_NOT_REACHABILITY = "Connection failed!. Please try again!"
let MESSAGE_TEXT___FOR_FUNCTIONALLITY_PENDING_MESSAGE = "We are still developing this feature. Thanks for your patience"
let MESSAGE_TEXT___DETAILS_NOT_VALID = "It seems either you are not registered with us or username/password are not correct. Please double check"
let MESSAGE_TEXT___SERVER_WITH_NAME_ALREADY_ADDED = "Server with the specified name is already added , please try again with different name."
let MESSAGE_TEXT___SERVER_WITH_IP_ALREADY_ADDED = "Server with the specified ip is already added , please try again with different ip."
let MESSAGE_TEXT___INPUT_MAC_ADDRESS_TO_TRACK = "Please Input the Mac address Of the device to Track"
let MESSAGE_TEXT___UNABLE_TO_LOGIN_WITH_THIS_SERVER = "It seems either 'SERVER Details' are not registered or username/password are not correct. Please double check."
let MESSAGE_TEXT___UNABLE_TO_TRACK_THIS_MAC_ADDRESS = "We are unable to track this device , either it is not active at the moment or it doesn't exist. please double check"
let MESSAGE_TEXT___STILL_PROCESSING_LOCATION_REQUEST = "Still processing , it may take few more seconds"


// constant notifications
let NOTIFICATION_CMX_SERVER_LIST_UPDATED = "NOTIFICATION_CMX_SERVER_LIST_UPDATED"
