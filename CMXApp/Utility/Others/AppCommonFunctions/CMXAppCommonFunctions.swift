//
//  WebServices.swift
//  CMXApp
//
//  Created by Cisco on 02/10/16.
//  Copyright © 2016 Cisco. All rights reserved.
//

//MARK: - CMXAppCommonFunctions : This singleton class implements some app specific functions which are frequently needed in application.

import Foundation
import UIKit
import Toaster
import Fabric
import Crashlytics
import CTFeedback
import IQKeyboardManagerSwift
import CMXSdk

//MARK: - Completion block
typealias CMXACFCompletionBlock = (_ returnedData :AnyObject?) ->()
typealias CMXACFModificationBlock = (_ viewControllerObject :AnyObject?) ->()

class CMXAppCommonFunctions: NSObject , UITextFieldDelegate {
    var completionBlock: CMXACFCompletionBlock?
    var navigationController: UINavigationController?
    var launchOptions:[AnyHashable: Any]?
    var macAddressInputTextField:UITextField?
    
    static let sharedInstance : CMXAppCommonFunctions = {
        let instance = CMXAppCommonFunctions()
        return instance
    }()
    
    fileprivate override init() {
        
    }
    
    func prepareForStartUp(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [AnyHashable: Any]?){
        self.launchOptions = launchOptions
        setupDatabase()
        performOnMainThreadWithOptimisation({[weak self] (returnedData) -> () in guard let `self` = self else { return }
            self.setupIQKeyboardManagerEnable()
            })
        setupOtherSettings()
        performOnMainThreadWithOptimisation({[weak self] (returnedData) -> () in guard let `self` = self else { return }
            self.setupCrashlytics()
            })
        showHomeScreen()
    }
    
    func setupDatabase() {
        CMXDatabaseManager.sharedInstance.setupCoreDataDatabase()
    }
    
    func setupKeyboardNextButtonHandler(){
        NotificationCenter.default.addObserver(self, selector: Selector(("viewLoadedNotification")), name: NSNotification.Name(rawValue: "viewLoaded"), object: nil)
    }
    
    func setupIQKeyboardManagerEnable(){
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = true
        IQKeyboardManager.sharedManager().shouldPlayInputClicks = true
    }
    
    func setupIQKeyboardManagerDisable(){
        IQKeyboardManager.sharedManager().enable = false
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = false
        IQKeyboardManager.sharedManager().shouldPlayInputClicks = false
    }
    
    func setupOtherSettings(){
        disableAutoCorrectionsAndTextSuggestionGlobally()
        navigationController = appDelegate.window?.rootViewController as? UINavigationController
        navigationController?.view.backgroundColor = UIColor.white
        windowObject()?.backgroundColor = UIColor.white
        if let tabBarController = appDelegate.window?.rootViewController as? UITabBarController {
            for vc in tabBarController.viewControllers! {
                vc.view.layoutIfNeeded()
            }
        }
    }
    
    func showHomeScreen(){
    }
    
    func setupCrashlytics(){
        Fabric.with([Crashlytics.self])
        updateUserInfoOnCrashlytics()
    }
    
    func updateUserInfoOnCrashlytics(){
        Crashlytics.sharedInstance().setUserName("")
        Crashlytics.sharedInstance().setUserIdentifier("")
    }
    
    func handleNotification(_ notification:NSDictionary?){
        if isNotNull(notification) {
            if let _ = notification as NSDictionary? {
                print("\n\nORIGINAL NOTIFICATION RECEIVED \n\(notification)\n\n")
            }
        }
    }
    
    func handleNotificationWhenTappedToView(_ notification:NSDictionary?){
    }
    
    func getViewController(_ identifier:NSString?)->(UIViewController){
        return storyBoardObject().instantiateViewController(withIdentifier: identifier as! String)
    }
    
    func presentVC(_ identifier:NSString? , viewController:UIViewController? , animated:Bool , modifyObject:CMXACFModificationBlock?){
        let vc = storyBoardObject().instantiateViewController(withIdentifier: identifier as! String)
        if let _ = modifyObject{
            modifyObject!(vc)
        }
        viewController!.present(UINavigationController(rootViewController: vc), animated: animated, completion: nil)
    }
    
    func pushVC(_ identifier:NSString?,navigationController:UINavigationController?,isRootViewController:Bool,animated:Bool,modifyObject:CMXACFModificationBlock?){
        let vc = storyBoardObject().instantiateViewController(withIdentifier: identifier as! String)
        if let _ = modifyObject{
            modifyObject!(vc)
        }
        if isRootViewController{
            navigationController!.setViewControllers([vc], animated: animated)
        }else{
            navigationController!.pushViewController(vc, animated: animated)
        }
    }
    
    func disableAutoCorrectionsAndTextSuggestionGlobally () {
        NotificationCenter.default.addObserver(self, selector: #selector(CMXAppCommonFunctions.notificationWhenTextViewDidBeginEditing(_:)), name: NSNotification.Name.UITextViewTextDidBeginEditing, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CMXAppCommonFunctions.notificationWhenTextFieldDidBeginEditing(_:)), name: NSNotification.Name.UITextFieldTextDidBeginEditing, object: nil)
    }
    
    func notificationWhenTextFieldDidBeginEditing (_ notification:Notification) {
        let textField = notification.object as? UITextField
        textField?.autocorrectionType = UITextAutocorrectionType.no
    }
    
    func notificationWhenTextViewDidBeginEditing (_ notification:Notification) {
        let textView = notification.object as? UITextView
        textView?.autocorrectionType = UITextAutocorrectionType.no
    }
    
    func addCMXServer(cmxServer:CMXServer){
        let serverInfo = NSMutableDictionary()
        copyData(cmxServer.name as AnyObject?, destinationDictionary: serverInfo, destinationKey: "name", methodName: #function)
        copyData(cmxServer.ip as AnyObject?, destinationDictionary: serverInfo, destinationKey: "ip", methodName: #function)
        copyData(cmxServer.userName as AnyObject?, destinationDictionary: serverInfo, destinationKey: "userName", methodName: #function)
        copyData(cmxServer.password as AnyObject?, destinationDictionary: serverInfo, destinationKey: "password", methodName: #function)
        copyData(cmxServer.version as AnyObject?, destinationDictionary: serverInfo, destinationKey: "version", methodName: #function)
        copyData(cmxServer.status as AnyObject?, destinationDictionary: serverInfo, destinationKey: "status", methodName: #function)
        var server = CMXDatabaseManager.sharedInstance.getServer(info: serverInfo, byKey: "name")
        if  server != nil {
            showToast(MESSAGE_TEXT___SERVER_WITH_NAME_ALREADY_ADDED);return
        }
        server = CMXDatabaseManager.sharedInstance.getServer(info: serverInfo, byKey: "ip")
        if  server != nil {
            showToast(MESSAGE_TEXT___SERVER_WITH_IP_ALREADY_ADDED);return
        }
        let servicesInfo = NSMutableArray()
        for service in cmxServer.services {
            let serviceInfo = NSMutableDictionary()
            copyData(service.dnsName as AnyObject?, destinationDictionary: serviceInfo, destinationKey: "dnsName", methodName: #function)
            copyData(service.lifeCycleStatus as AnyObject?, destinationDictionary: serviceInfo, destinationKey: "lifeCycleStatus", methodName:#function)
            copyData(service.nodeName as AnyObject?, destinationDictionary: serviceInfo, destinationKey: "nodeName", methodName:#function)
            copyData(service.port as AnyObject?, destinationDictionary: serviceInfo, destinationKey: "port", methodName:#function)
            copyData(service.properties as AnyObject?, destinationDictionary: serviceInfo, destinationKey: "properties", methodName:#function)
            copyData(service.serviceName as AnyObject?, destinationDictionary: serviceInfo, destinationKey: "serviceName", methodName:#function)
            copyData(service.type as AnyObject?, destinationDictionary: serviceInfo, destinationKey: "type", methodName:#function)
            copyData(service.weight as AnyObject?, destinationDictionary: serviceInfo, destinationKey: "weight", methodName:#function)
            servicesInfo.add(serviceInfo)
        }
        CMXDatabaseManager.sharedInstance.addServer(info: serverInfo, services: servicesInfo as! [NSDictionary])
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NOTIFICATION_CMX_SERVER_LIST_UPDATED), object: nil)
    }
    
    func getMacAddressAsInput(viewController:UIViewController,completionBlock:@escaping CMXACFCompletionBlock){
        let prompt = UIAlertController(title: "Tracking", message: MESSAGE_TEXT___INPUT_MAC_ADDRESS_TO_TRACK, preferredStyle: UIAlertControllerStyle.alert)
        prompt.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
        prompt.addAction(UIAlertAction(title: "Track It", style: UIAlertActionStyle.default, handler: { (action) -> Void in
            var enteredText = (prompt.textFields![0] as UITextField).text
            if enteredText != nil {
                enteredText = enteredText?.replacingOccurrences(of: ":", with: "")
                enteredText = self.insert(seperator: ":", afterEveryXChars: 2, intoString: enteredText!)
                userDefaults.set(enteredText, forKey: "lastEnteredMacAddress")
            }
            if validateMacAddress(enteredText as NSString?, identifier: "Mac Address"){
                completionBlock(enteredText! as AnyObject?)
            }else{
                DispatchQueue.main.async {
                    self.getMacAddressAsInput(viewController: viewController, completionBlock: completionBlock)
                }
            }
            }
        ))
        prompt.addTextField(configurationHandler: {(textField: UITextField!) in
            textField.placeholder = "eg:- a4:08:ea:8c:01:56"
            textField.keyboardType = UIKeyboardType.emailAddress
            if let lastEnteredMAcAddress = userDefaults.object(forKey: "lastEnteredMacAddress") as? String {
                textField.text = lastEnteredMAcAddress
            }
            self.macAddressInputTextField = textField
            self.macAddressInputTextField?.delegate = self
        })
        viewController.presentVC(prompt)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        if textField == self.macAddressInputTextField {
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(CMXAppCommonFunctions.correctMacAddress), object: nil)
            self.perform(#selector(CMXAppCommonFunctions.correctMacAddress), with: nil, afterDelay: 1)
        }
        return true
    }
    
    func correctMacAddress(){
        var text = NSMutableString(string: safeString(object: self.macAddressInputTextField!.text))
        if text.length > 0{
            text = NSMutableString(string: safeString(object:text.replacingOccurrences(of: ":", with: "")))
            self.macAddressInputTextField?.text = self.insert(seperator: ":", afterEveryXChars: 2, intoString: text.string)
        }
    }
    
    func insert(seperator: String, afterEveryXChars: Int, intoString: String) -> String {
        var output = ""
        intoString.characters.enumerated().forEach { index, c in
            if index % afterEveryXChars == 0 && index > 0 {
                output += seperator
            }
            output.append(c)
        }
        return output
    }
    
}
