//
//  CMXCacheManager.swift
//  CMXApp
//
//  Created by Cisco on 02/10/16.
//  Copyright © 2016 Cisco. All rights reserved.
//

import Foundation

let CACHE_MANAGER_NAME = "cachemanager"

class CMXCacheManager: NSObject  {

    static let sharedInstance : CMXCacheManager = {
        let instance = CMXCacheManager()
        return instance
    }()
    
    func resetDatabase(){
        do{
            try FileManager.default.removeItem(atPath: CMXCacheManager.pathInDocDirectory(filename: CACHE_MANAGER_NAME)!)
        }catch {
            print("EXCEPTION WHEN TRYING TO RESET CACHE MANAGER")
        }
    }

    private var objectMaxId: Int = 0
    private var filenameFromIdDico: [String : String] = [:]
    
    override init(){
        super.init()
    }
    
    required init(coder aDecoder: NSCoder) {
        self.objectMaxId = aDecoder.decodeInteger(forKey: "objectMaxId")
        if let dico:[String:String] = aDecoder.decodeObject(forKey: "filenameFromUrlDic") as? [String:String] {
            self.filenameFromIdDico = dico
        }
        super.init()
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encode(self.objectMaxId, forKey: "objectMaxId")
        aCoder.encode(self.filenameFromIdDico, forKey: "filenameFromUrlDic")
    }
    
    class func pathInDocDirectory(filename: String)->String? {
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        if paths.count > 0 {
            if let path: String = paths[0] as? String {
                return path + "/" + filename
            }
        }
        return nil
    }
    
    private func saveToDevice() {
        if let path = CMXCacheManager.pathInDocDirectory(filename: CACHE_MANAGER_NAME) {
            NSKeyedArchiver.archiveRootObject(self, toFile: path)
        }
    }
    
    func saveObject(object:AnyObject?, identifier:String) -> Bool {
        if (identifier.isEmpty) {
            return false
        }
        objc_sync_enter(self)
        var filename: String
        if let filenameFromDico: String = self.filenameFromIdDico[identifier] {
            filename = filenameFromDico
        }
        else {
            self.objectMaxId += 1
            filename = "object." + String(self.objectMaxId)
            self.filenameFromIdDico[identifier] = filename
        }
        objc_sync_exit(self)
        var status: Bool = false
        if let filepath: String = CMXCacheManager.pathInDocDirectory(filename: filename) {
            if let _ = object{
                NSKeyedArchiver.archiveRootObject(object!, toFile: filepath)
                status = true
            }else{
                do{
                    try FileManager.default.removeItem(atPath: filepath)
                } catch {
                    
                }
            }
        }
        self.saveToDevice()
        return status;
    }
    
    func loadObject(identifier:String) -> AnyObject? {
        if let filename: String = self.filenameFromIdDico[identifier] {
            if let filepath = CMXCacheManager.pathInDocDirectory(filename: filename) {
                return NSKeyedUnarchiver.unarchiveObject(withFile: filepath) as AnyObject?
            }
        }
        return nil
    }
    
}
